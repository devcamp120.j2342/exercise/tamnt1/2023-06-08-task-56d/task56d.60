package com.example.customerinvoiceapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customerinvoiceapi.models.Customer;
import com.example.customerinvoiceapi.services.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService service;

    @GetMapping("/customers")
    public List<Customer> getCustomerList() {
        return service.createCustomer();
    }
}
