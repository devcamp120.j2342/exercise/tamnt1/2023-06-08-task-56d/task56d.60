package com.example.customerinvoiceapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.customerinvoiceapi.models.Invoice;
import com.example.customerinvoiceapi.services.InvoiceService;

@RestController
@CrossOrigin
public class InvoiceController {
    @Autowired
    private InvoiceService service;

    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoices() {
        return service.createInvoice();
    }

    @GetMapping("/invoices/{invoiceId}")
    public Invoice getInvoices(@PathVariable("invoiceId") int invoiceId) {
        return service.getInvoicebyIndex(invoiceId);
    }
}
