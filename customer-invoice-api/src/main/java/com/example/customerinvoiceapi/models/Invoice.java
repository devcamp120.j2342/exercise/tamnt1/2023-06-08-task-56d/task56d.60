package com.example.customerinvoiceapi.models;

public class Invoice {
    private int id;
    private Customer customer;
    private double amount;

    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAccount() {
        return amount;
    }

    public void setAccount(double amount) {
        this.amount = amount;
    }

    public String getCustomerName() {
        return this.customer.getName();

    }

    public double getAmountAdterDiscount(int discount) {
        return this.amount * discount;

    }

    @Override
    public String toString() {
        return "Invoice [id=" + id + ", customer=" + customer + ", account=" + amount + "]";
    }

}
