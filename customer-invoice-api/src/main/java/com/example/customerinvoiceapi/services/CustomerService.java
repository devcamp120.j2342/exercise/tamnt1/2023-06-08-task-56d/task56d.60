package com.example.customerinvoiceapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {
    public List<Customer> createCustomer() {
        ArrayList<Customer> customerList = new ArrayList<>();
        Customer customer1 = new Customer(1, "Tam", 10);
        Customer customer2 = new Customer(2, "adam", 20);
        Customer customer3 = new Customer(3, "Khan", 20);
        Customer customer4 = new Customer(4, "Minh", 10);
        Customer customer5 = new Customer(5, "Thuy", 20);
        Customer customer6 = new Customer(6, "Ha", 20);

        customerList.addAll(Arrays.asList(customer1, customer2, customer3, customer4, customer5, customer6));

        return customerList;
    }
}
