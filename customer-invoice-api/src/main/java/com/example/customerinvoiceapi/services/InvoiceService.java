package com.example.customerinvoiceapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.customerinvoiceapi.models.Customer;
import com.example.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    public ArrayList<Invoice> createInvoice() {
        ArrayList<Customer> customerList = (ArrayList<Customer>) customerService.createCustomer();
        ArrayList<Invoice> invoiceList = new ArrayList<>();

        Customer customer1 = customerList.get(0);
        Customer customer2 = customerList.get(1);
        Customer customer3 = customerList.get(2);
        Customer customer4 = customerList.get(3);
        Customer customer5 = customerList.get(4);
        Customer customer6 = customerList.get(5);
        Invoice invoice1 = new Invoice(1, customer1, 10000);
        Invoice invoice2 = new Invoice(1, customer2, 20000);
        Invoice invoice3 = new Invoice(1, customer3, 30000);
        Invoice invoice4 = new Invoice(1, customer4, 10000);
        Invoice invoice5 = new Invoice(1, customer5, 20000);
        Invoice invoice6 = new Invoice(1, customer6, 30000);
        invoiceList.addAll(Arrays.asList(invoice1, invoice2, invoice3, invoice4, invoice5, invoice6));

        return invoiceList;

    }

    public Invoice getInvoicebyIndex(int idx) {
        ArrayList<Invoice> invoiceList = createInvoice();
        for (int index = 0; index < invoiceList.size(); index++) {
            if (index == idx && idx < invoiceList.size() && idx > 0) {
                return invoiceList.get(index);
            }
        }

        return null;
    }
}
